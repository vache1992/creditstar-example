<?php


namespace tests\models;

use app\models\users\User;
use DateTime;
use DateTimeZone;
use PHPUnit\Framework\TestCase;

class AgeTest extends TestCase
{
    public function testAgeByPersonalCode()
    {

        $personalCode = '39311110433';

        $g = [
            1 => '18',
            2 => '18',
            3 => '19',
            4 => '19',
            5 => '20',
            6 => '20',
        ];

        $year = $g[$personalCode[0]] . $personalCode[1] . $personalCode[2];
        $month = $personalCode[3] . $personalCode[4];
        $day = $personalCode[5] . $personalCode[6];
        $userDate = $year . '-' . $month . '-' . $day;

        $tz  = new DateTimeZone('Asia/Yerevan');

        $ageT = DateTime::createFromFormat('Y-m-d', $userDate, $tz)
            ->diff(new DateTime('now', $tz))
            ->y;

        $user = new User();
        $age = $user->getUserAgeByUserPersonalCode($personalCode);

        $this->assertEquals($ageT, $age, 'Method work incorrect');

        $this->assertTrue($ageT >= 18, 'User is underage');

    }
}