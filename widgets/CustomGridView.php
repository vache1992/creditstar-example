<?php
namespace app\widgets;

use yii\grid\GridView;

class CustomGridView extends GridView
{

    public $tableOptions = ['class' => 'table custom_table'];

}