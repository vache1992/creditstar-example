<?php
namespace app\widgets;

use yii\widgets\DetailView;

class CustomDetailView extends DetailView
{

    public $options = ['class' => 'table custom_table_view table-bordered'];

}