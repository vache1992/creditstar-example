<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\LayoutAsset;
use app\widgets\Alert;
use app\widgets\CustomBreadcrumbs;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
LayoutAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<?=$this->render('_topNavigation')?>
<div class="breadcrumb-container">
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <ol class="breadcrumb">
                    <li>
                        <div class="media">
                            <div class="media-left">
                                <img src="<?= Yii::$app->request->baseUrl ?>/images/header-logo.png" >
                            </div>
                            <div class="media-body">
                                <?= CustomBreadcrumbs::widget([
                                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                                ]) ?>
                            </div>
                        </div>
                    </li>
                </ol>
            </div>
            <div class="col-md-2 language">
                <span>russian</span>
            </div>
        </div>
    </div>
</div>
<?=$this->render('_menu')?>

<div class="wrap">

    <div class="container">
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
