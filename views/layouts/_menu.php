<?php

use yii\helpers\Html;
use yii\helpers\Url;

$controllerName = Yii::$app->controller->id;

?>
<div class="menu-container">
    <nav class="navbar navbar-default">
        <div class="container">
            <ul class="nav navbar-nav">
                <li class="<?php echo ($controllerName == 'site') ? 'active' : ''; ?>">
                    <?= Html::a('Home', Url::toRoute('/', true), ['class' => '']) ?>
                </li>
                <li class="<?php echo ($controllerName == 'user') ? 'active' : ''; ?>" >
                    <?= Html::a('User', Url::toRoute('/user', true), ['class' => '']) ?>
                </li>
                <li class="<?php echo ($controllerName == 'loan') ? 'active' : ''; ?>">
                    <?= Html::a('Loan', Url::toRoute('/loan', true), ['class' => '']) ?>
                </li>
            </ul>
        </div>
    </nav>
</div>