<div class="bg-dark">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <ul class="list-unstyled list-inline pt-5 sm-center">
                    <li>
                        Klienditeninus
                    </li>
                    <li>
                        <span><i class="fa fa-mobile-alt"></i> 1715</span>
                    </li>
                    <li>
                        <span><i class="far fa-clock"></i> E-P 9.00-21.00</span>
                    </li>
                </ul>
            </div>
            <div class="col-sm-6 text-right ">
                <ul class="list-unstyled list-inline pt-5 sm-center">
                    <li>
                        Tere, Kaupo Kasutaja
                    </li>
                    <li>
                        <button class="button-orange">
                            <i class="fas fa-lock-open"></i> LOG OUT
                        </button>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

