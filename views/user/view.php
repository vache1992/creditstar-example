<?php

use app\widgets\CustomDetailView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\users\User */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="user-view">

    <div class="border-1 p-10 rounded-4">
        <h1><?= Html::encode($this->title) ?></h1>

        <?= CustomDetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'first_name:ntext',
                'last_name:ntext',
                'email:ntext',
                'personal_code',
                'phone',
                'active:boolean',
                'dead:boolean',
                'lang:ntext',
            ],
        ]) ?>
    </div>

    <div class="pt-10">
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'button-large button-orange']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'button-large button-red',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </div>
</div>
