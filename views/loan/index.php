<?php

use app\widgets\CustomGridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\loans\LoanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Loans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loan-index">
    <div class="border-1 p-10 rounded-4">

        <h1><?= Html::encode($this->title) ?></h1>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= CustomGridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'summary' => '',
            'columns' => [
                'id',
                'user_id',
                'amount',
                'interest',
                'duration',
                //'start_date',
                //'end_date',
                //'campaign',
                //'status:boolean',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header'=>'<a href="javascript:void(0);"><i class="fa fa-cogs"></i></a>',
                    'template' => '{view} {update} {delete} {myButton}',
                    'buttons' => [
                        'view' => function ($url, $model, $key) {
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['class' => 'button-small button-orange']);
                        },
                        'update' => function ($url, $model, $key) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['class' => 'button-small button-orange']);
                        },
                        'delete' => function ($url, $model, $key) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'class' => 'button-small button-orange',
                                'data' => [
                                    'confirm' => 'Are you absolutely sure ? You will lose all the information about this user with this action.',
                                    'method' => 'post',
                                ],
                            ]);
                        },
                    ],
                ],
            ],
        ]); ?>
    </div>

    <div class="pt-10">
        <?= Html::a('Create Loan', ['create'], ['class' => 'button-large button-orange']) ?>
    </div>

</div>