<?php

use app\widgets\CustomDetailView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\loans\Loan */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Loans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="loan-view">
    <div class="border-1 p-10 rounded-4">

        <h1><?= Html::encode($this->title) ?></h1>

        <?= CustomDetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'user_id',
                'amount',
                'interest',
                'duration',
                'start_date',
                'end_date',
                'campaign',
                'status:boolean',
            ],
        ]) ?>
    </div>

    <div class="pt-10">
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'button-large button-orange']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'button-large button-red',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </div>

</div>
