<?php

use yii\db\Migration;
use yii\helpers\BaseJson;

/**
 * Handles the creation of table `{{%loans}}`.
 */
class m200320_184026_create_loans_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%loans}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->bigInteger()->notNull(),
            'amount' => $this->decimal(10, 2)->notNull(),
            'interest' => $this->decimal(10, 2)->notNull(),
            'duration' => $this->integer()->notNull(),
            'start_date' => $this->date()->notNull(),
            'end_date' => $this->date()->notNull(),
            'campaign' => $this->integer()->notNull(),
            'status' => $this->integer()
        ]);


        $dataJson = file_get_contents(\Yii::getAlias('@app') . '/loans.json');
        $data = BaseJson::decode($dataJson);
        foreach ($data as &$value) {
            $value['start_date'] = date('Y-m-d h:i:s', $value['start_date']);
            $value['end_date'] = date('Y-m-d h:i:s', $value['end_date']);
        }
        $this->batchInsert('loans', ['id', 'user_id', 'amount', 'interest', 'duration', 'start_date', 'end_date', 'campaign', 'status'], $data);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%loans}}');
    }
}
