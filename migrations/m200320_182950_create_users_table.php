<?php

use yii\db\Migration;
use yii\helpers\BaseJson;

/**
 * Handles the creation of table `{{%users}}`.
 */
class m200320_182950_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%users}}', [
            'id' => $this->primaryKey(),
            'first_name' => $this->text()->notNull(),
            'last_name' => $this->text()->notNull(),
            'email' => $this->text()->notNull(),
            'personal_code' => $this->bigInteger()->notNull(),
            'phone' => $this->bigInteger()->notNull(),
            'active' => $this->boolean(),
            'dead' => $this->boolean(),
            'lang' => $this->text()
        ]);

        $dataJson = file_get_contents(\Yii::getAlias('@app') . '/users.json');
        $data = BaseJson::decode($dataJson);
        $this->batchInsert('users', ['id', 'first_name', 'last_name', 'email', 'personal_code', 'phone', 'active', 'dead', 'lang'], $data);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%users}}');
    }
}
