<?php

namespace app\models\users;

use DateTime;
use DateTimeZone;
use Yii;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property int $personal_code
 * @property int $phone
 * @property bool|null $active
 * @property bool|null $dead
 * @property string|null $lang
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email', 'personal_code', 'phone'], 'required'],
            [['first_name', 'last_name', 'email', 'lang'], 'string'],
            [['personal_code', 'phone'], 'default', 'value' => null],
            [['personal_code', 'phone'], 'integer'],
            [['active', 'dead'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'personal_code' => 'Personal Code',
            'phone' => 'Phone',
            'active' => 'Active',
            'dead' => 'Dead',
            'lang' => 'Lang',
        ];
    }

    public function getUserAgeByUserPersonalCode(int $personalCode): int {

        $personalCode = (string)$personalCode;
        $g = [
            1 => '18',
            2 => '18',
            3 => '19',
            4 => '19',
            5 => '20',
            6 => '20',
        ];

        $year = $g[$personalCode[0]] . $personalCode[1] . $personalCode[2];
        $month = $personalCode[3] . $personalCode[4];
        $day = $personalCode[5] . $personalCode[6];
        $userDate = $year . '-' . $month . '-' . $day;

        $tz  = new DateTimeZone('Asia/Yerevan');

        $age = DateTime::createFromFormat('Y-m-d', $userDate, $tz)
            ->diff(new DateTime('now', $tz))
            ->y;

        return $age;

    }
}
